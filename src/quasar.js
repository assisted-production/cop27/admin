import Vue from "vue";

import "./styles/quasar.scss";
import "quasar/dist/quasar.ie.polyfills";
import "@quasar/extras/material-icons/material-icons.css";
import { Quasar, Meta, Loading, Notify, Dialog } from "quasar";

Vue.use(Quasar, {
  config: {},
  plugins: {
    Meta,
    Loading,
    Notify,
    Dialog
  }
});
